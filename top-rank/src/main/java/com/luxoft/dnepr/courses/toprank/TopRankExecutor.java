package com.luxoft.dnepr.courses.toprank;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 19.05.13
 * Time: 18:03
 * To change this template use File | Settings | File Templates.
 */
public class TopRankExecutor {
    private double dampingFactor;
    private int numberOfLoopsInRankComputing;


    public TopRankExecutor(double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
    }

    public TopRankResults execute(Map<String, String> urlContent) {
        TopRankResults results = new TopRankResults();
        Map<String, List<String>> graph = new HashMap();
        Map<String, List<String>> reverseGraph = new HashMap();
        Map<String, List<String>> index = new HashMap();
        Map<String, Double> ranks = new HashMap();

        index = indexCreate(urlContent);
        graph = graphCreate(urlContent);
        reverseGraph = reverseGraphCreate(urlContent);

        ranks = ranksCreate(graph, reverseGraph);
        results.setGraph(graph);
        results.setIndex(index);
        results.setRanks(ranks);
        results.setReverseGraph(reverseGraph);
        return results;
    }

    private Map<String, Double> ranksCreate(Map<String, List<String>> graph, Map<String, List<String>> reverseGraph) {
        Map<String, Double> ranks = new HashMap();
        Set<String> urlSet = graph.keySet();

        for (String url : urlSet) {
            ranks.put(url, 1.0 / urlSet.size());
        }

        for (int i = 0; i < this.numberOfLoopsInRankComputing; i++) {
            Map<String, Double> ranksTemp = new HashMap<>(ranks);
            Double rank;
            for (String url : urlSet) {
                rank = (1 - this.dampingFactor) / urlSet.size();
                for (String link : graph.get(url)) {
                    rank += this.dampingFactor * (ranksTemp.get(link) / reverseGraph.get(link).size());
                }
                ranks.put(url, rank);
            }
        }
        return ranks;

    }


    private Map<String, List<String>> graphCreate(Map<String, String> urlContent) {
        Map<String, List<String>> map = new HashMap<>();
        Set<String> urlSet = urlContent.keySet();
        for (String url : urlSet) {
            List<String> list = new ArrayList<>();
            for (String url2 : urlSet) {
                String ss = "<a href=" + "\"" + url + "\">" + "[\\W\\w]+</a>";
                Pattern pattern = Pattern.compile(ss);
                Matcher m = pattern.matcher(urlContent.get(url2));
                if (m.find())
                    list.add(url2);
            }
            map.put(url, list);
        }
        return map;
    }

    private Map<String, List<String>> indexCreate(Map<String, String> urlContent) {
        Map<String, List<String>> map = new HashMap<>();
        String[] word;
        List<String[]> words = new ArrayList<>();
        Set<String> url = urlContent.keySet();
        Iterator<String> urlIterator = url.iterator();
        String stringUrl;
        Set<String> setWord = new HashSet<>();

        while (urlIterator.hasNext()) {
            stringUrl = urlIterator.next();
            word = urlContent.get(stringUrl).split("[\n\\ \\t]");
            for (int i = 0; i < word.length; i++)
                setWord.add(word[i]);
            words.add(word);
        }

        map = createIndexMap(setWord, url, words);
        return map;
    }

    private Map<String, List<String>> createIndexMap(Set<String> setWord, Set<String> url, List<String[]> words) {
        Map<String, List<String>> map = new HashMap<>();
        String stringUrl;
        String str;
        Iterator<String> setWordIterator = setWord.iterator();
        while (setWordIterator.hasNext()) {
            List<String> list = new ArrayList<>();
            str = setWordIterator.next();
            Iterator<String> urlIterator = url.iterator();
            for (int i = 0; i < words.size(); i++) {
                stringUrl = urlIterator.next();
                for (int j = 0; j < words.get(i).length; j++) {
                    if (words.get(i)[j].equals(str))
                        if (!list.contains(stringUrl))
                            list.add(stringUrl);
                }
            }
            map.put(str, list);
        }
        return map;
    }

    private Map<String, List<String>> reverseGraphCreate(Map<String, String> urlContent) {
        Map<String, List<String>> reverseGraph = new HashMap<>();
        Set<String> urlSet = urlContent.keySet();

        for (String url : urlSet) {
            List<String> list = new ArrayList<>();

            for (String url2 : urlSet) {
                String link = "<a href=" + "\"" + url2 + "\">" + "[\\W\\w]+</a>";
                Pattern pattern = Pattern.compile(link);
                Matcher matcher = pattern.matcher(urlContent.get(url));
                if (matcher.find()) {
                    list.add(url2);
                }
            }
            reverseGraph.put(url, list);
        }
        return reverseGraph;
    }

}

