package com.luxoft.dnepr.courses.toprank;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 30.05.13
 * Time: 11:30
 * To change this template use File | Settings | File Templates.
 */
public class TopRankSearcher {
    private static final double DEFAULT_DAMPING_FACTOR = 0.8;
    private static final int DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING = 10;
    private double dampingFactor;
    private int numberOfLoopsInRankComputing;
    private Map<String, String> urlContent = new ConcurrentHashMap<>();
    private TopRankResults results = new TopRankResults();

    public TopRankResults execute(List<String> urls) {
        return execute(urls, DEFAULT_DAMPING_FACTOR, DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING);
    }

    public TopRankResults execute(List<String> urls, double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
        Map<String, Double> ranks = new HashMap();

        ExecutorService urlExecutor = Executors.newCachedThreadPool();
        for (String link : urls) {
            urlExecutor.execute(new urlContentCreate(link));
        }
        urlExecutor.shutdown();
        while (!urlExecutor.isTerminated()) {
        }

        ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(new GraphCreate());
        executor.execute(new ReverseGraphCreate());
        executor.execute(new IndexCreate());
        executor.shutdown();
        while (!executor.isTerminated()) {
        }

        ranks = ranksCreate(this.results.getGraph(), this.results.getReverseGraph());
        results.setRanks(ranks);

        return results;
    }


    class urlContentCreate implements Runnable {
        private String link;

        urlContentCreate(String link) {
            this.link = link;
        }

        @Override
        public void run() {
            try {
                URL url = URI.create(link).toURL();
                URLConnection connection;
                connection = url.openConnection();
                connection.connect();
                StringBuffer sb = new StringBuffer();
                try (Scanner scanner = new Scanner(connection.getInputStream())) {
                    while (scanner.hasNextLine()) {
                        sb.append(scanner.nextLine() + "\n");
                    }
                    urlContent.put(link, sb.toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, Double> ranksCreate(Map<String, List<String>> graph, Map<String, List<String>> reverseGraph) {
        Map<String, Double> ranks = new HashMap();
        Set<String> urlSet = graph.keySet();

        for (String url : urlSet) {
            ranks.put(url, 1.0 / urlSet.size());
        }

        for (int i = 0; i < this.numberOfLoopsInRankComputing; i++) {
            Map<String, Double> ranksTemp = new HashMap<>(ranks);
            Double rank;
            for (String url : urlSet) {
                rank = (1 - this.dampingFactor) / urlSet.size();
                for (String link : graph.get(url)) {
                    rank += this.dampingFactor * (ranksTemp.get(link) / reverseGraph.get(link).size());
                }
                ranks.put(url, rank);
            }
        }
        return ranks;

    }

    class GraphCreate implements Runnable {
        Map<String, List<String>> graph = new HashMap<>();

        @Override
        public void run() {
            for (String url : urlContent.keySet()) {
                List<String> list = new ArrayList<>();
                for (String url2 : urlContent.keySet()) {
                    String link = "<a href=" + "\"" + url + "\">" + "[\\W\\w]+</a>";
                    Pattern pattern = Pattern.compile(link);
                    Matcher matcher = pattern.matcher(urlContent.get(url2));
                    if (matcher.find()) {
                        list.add(url2);
                    }
                }
                graph.put(url, list);
            }
            results.setGraph(graph);
        }
    }


    class IndexCreate implements Runnable {
        Map<String, List<String>> map = new HashMap<>();
        String[] word;
        List<String[]> words = new ArrayList<>();

        @Override
        public void run() {
            Set<String> url = urlContent.keySet();
            Iterator<String> urlIterator = url.iterator();
            String stringUrl;
            Set<String> setWord = new HashSet<>();
            while (urlIterator.hasNext()) {
                stringUrl = urlIterator.next();
                word = urlContent.get(stringUrl).split("[\n\\ \\t]");
                for (int i = 0; i < word.length; i++)
                    setWord.add(word[i]);
                words.add(word);
            }
            map = createIndexMap(setWord, url, words);
            results.setIndex(map);
        }
    }

    private Map<String, List<String>> createIndexMap(Set<String> setWord, Set<String> url, List<String[]> words) {
        Map<String, List<String>> map = new HashMap<>();
        String stringUrl;
        String str;
        Iterator<String> setWordIterator = setWord.iterator();
        while (setWordIterator.hasNext()) {
            List<String> list = new ArrayList<>();
            str = setWordIterator.next();
            Iterator<String> urlIterator = url.iterator();
            for (int i = 0; i < words.size(); i++) {
                stringUrl = urlIterator.next();
                for (int j = 0; j < words.get(i).length; j++) {
                    if (words.get(i)[j].equals(str))
                        if (!list.contains(stringUrl))
                            list.add(stringUrl);
                }
            }
            map.put(str, list);
        }
        return map;
    }

    class ReverseGraphCreate implements Runnable {
        Map<String, List<String>> reverseGraph = new HashMap<>();
        Set<String> urlSet = urlContent.keySet();

        @Override
        public void run() {
            for (String url : urlSet) {
                List<String> list = new ArrayList<>();
                for (String url2 : urlSet) {
                    String link = "<a href=" + "\"" + url2 + "\">" + "[\\W\\w]+</a>";
                    Pattern pattern = Pattern.compile(link);
                    Matcher matcher = pattern.matcher(urlContent.get(url));
                    if (matcher.find()) {
                        list.add(url2);
                    }
                }
                reverseGraph.put(url, list);
            }
            results.setReverseGraph(reverseGraph);
        }
    }

}
