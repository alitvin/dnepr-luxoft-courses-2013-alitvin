package com.luxoft.courses.unit13;

import oracle.jrockit.jfr.settings.JSONElement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 15.06.13
 * Time: 18:42
 * To change this template use File | Settings | File Templates.
 */

@WebServlet(name = "ServletPas")

public class ServletPas extends HttpServlet {
    HashMap<String, String> map = new HashMap<String, String>();
    ArrayList<String> list = new ArrayList<String>();


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String paramLogin = request.getParameter("login");
        String paramPassword = request.getParameter("password");
        namePassword(getServletContext().getInitParameter("users"));
        if (map.containsKey(paramLogin) && map.get(paramLogin).equals(paramPassword)) {
            HttpSession session = request.getSession();
            session.setAttribute("name", paramLogin);
            response.sendRedirect("user");
        } else {
            response.sendRedirect("index.html?param1=error");
        }
    }

    private void namePassword(String users) {
        boolean flag = false;
        for (int i = 0; i < users.length(); i++) {
            if (users.charAt(i) == '\"') {
                flag = true;
                i++;
                StringBuilder str = new StringBuilder();
                while (flag) {
                    str.append(users.charAt(i));
                    i++;
                    if (users.charAt(i) == '\"')
                        flag = false;
                }
                list.add(str.toString());
            }
        }
        for (int i = 0; i < list.size(); i += 2) {
            map.put(list.get(i), list.get(i + 1));
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
