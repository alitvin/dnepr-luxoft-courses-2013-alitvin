package com.luxoft.courses.unit13;

import com.sun.org.apache.bcel.internal.generic.ACONST_NULL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 12.06.13
 * Time: 23:02
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name = "ServletDummy")
public class ServletDummy extends HttpServlet {
    static ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String paramValueName = request.getHeader("name");
        String paramValueAge = request.getHeader("age");
        PrintWriter writer = response.getWriter();

        if (paramValueAge == null || paramValueName == null) {
            writer.println("HTTP/1.1 200 500 Internal Server Error");
            writer.println("Content-Type: application/json; charset=utf-8");
            writer.println("Content-Length: " + 97);
            writer.println("error: Illegal parameters");
        } else if (map.containsKey(paramValueName)) {
            writer.println("HTTP/1.1 500 Internal Server Error");
            writer.println("Content-Type: application/json; charset=utf-8");
            writer.println("Content-Length: " + 97);
            writer.println("error: Name " + paramValueName + " already exists");
        } else {
            writer.println("HTTP/1.1 201 Created");
            map.put(paramValueName, paramValueAge);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String paramValueName = request.getHeader("name");
        String paramValueAge = request.getHeader("age");
        PrintWriter writer = response.getWriter();

        if (paramValueAge == null || paramValueName == null) {
            writer.println("HTTP/1.1 200 500 Internal Server Error");
            writer.println("Content-Type: application/json; charset=utf-8");
            writer.println("Content-Length: " + 97);
            writer.println("error: Illegal parameters");
        } else if (!map.containsKey(paramValueName)) {
            writer.println("HTTP/1.1 500 Internal Server Error");
            writer.println("Content-Type: application/json; charset=utf-8");
            writer.println("Content-Length: " + 97);
            writer.println("error: Name " + paramValueName + " does not exist");
        } else {
            writer.println("HTTP/1.1 202 Accepted");
            map.put(paramValueName, paramValueAge);
        }


    }


}
