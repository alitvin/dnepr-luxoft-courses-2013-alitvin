package com.luxoft.courses.unit13;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 18.06.13
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name = "ServletUser")
public class ServletUser extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession ses = request.getSession(false);
        if (ses == null)
            response.sendRedirect("index.html");
        else {
            String user = (String) ses.getAttribute("name");
            response.setContentType("text/html");
            PrintWriter writer = response.getWriter();
            writer.println("Hello " + user + "!");
            writer.println("<p><a href=\"log\">logout</a>");
        }
    }
}
