package com.luxoft.courses.unit13;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 11.06.13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name = "ServletCount")
public class ServletCount extends HttpServlet {
    static AtomicInteger count = new AtomicInteger(0);


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        count.incrementAndGet();
        response.setStatus(HttpServletResponse.SC_OK);
        int l = 97;


        response.setIntHeader("hitCount", count.get());
        PrintWriter writer = response.getWriter();
        writer.println("HTTP/1.1 200 OK");
        writer.println("Content-Type: application/json; charset=utf-8");
        writer.println("Content-Length: " + l);
        writer.println("hitCount: " + count.get());


    }
}
