package com.luxoft.courses.unit15.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 25.06.13
 * Time: 11:33
 * To change this template use File | Settings | File Templates.
 */
@WebFilter(filterName = "FilterLog")
public class FilterLog implements Filter {


    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {


        if (((HttpServletRequest) req).getSession(false) == null) {

            ((HttpServletResponse) resp).sendRedirect("/myFirstWebApp/index.html");
            chain.doFilter(req, resp);
        }

        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }


}
