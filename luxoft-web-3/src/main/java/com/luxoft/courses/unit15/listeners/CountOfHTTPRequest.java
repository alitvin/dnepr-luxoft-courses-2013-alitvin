package com.luxoft.courses.unit15.listeners;

import com.luxoft.courses.unit15.Constants;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionEvent;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 30.06.13
 * Time: 12:23
 * To change this template use File | Settings | File Templates.
 */
public class CountOfHTTPRequest implements ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        getActiveSessions(sre).getAndIncrement();
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        getActiveSessions(sre).getAndIncrement();
        getActiveSessions1(sre).getAndIncrement();

    }
    private AtomicLong getActiveSessions(ServletRequestEvent sre) {
        return (AtomicLong) sre.getServletRequest().getServletContext().getAttribute(Constants.COUNT_OF_HTTP_REQUEST);
    }
    private AtomicLong getActiveSessions1(ServletRequestEvent sre) {
        return (AtomicLong) sre.getServletRequest().getAttribute(Constants.COUNT_OF_HTTP_REQUEST);
    }
}
