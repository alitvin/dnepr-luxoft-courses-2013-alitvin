package com.luxoft.courses.unit15.listeners;

import com.luxoft.courses.unit15.Constants;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 30.06.13
 * Time: 10:04
 * To change this template use File | Settings | File Templates.
 */
public class ActiveSessionCountHttpSessionListener implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent hse) {

        getActiveSessions(hse).getAndIncrement();
        if (hse.getSession().getServletContext().getAttribute("role").equals("admin"))
            getActiveAdminSessions(hse).getAndIncrement();
        else
            getActiveUserSessions(hse).getAndIncrement();




    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndDecrement();

    }

    private AtomicLong getActiveSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
    }
    private AtomicLong getActiveAdminSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE_ADMIN);
    }
    private AtomicLong getActiveUserSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE_USER);
    }

}
