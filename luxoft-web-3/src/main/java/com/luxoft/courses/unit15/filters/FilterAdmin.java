package com.luxoft.courses.unit15.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 29.06.13
 * Time: 23:46
 * To change this template use File | Settings | File Templates.
 */
@WebFilter(filterName = "FilterAdmin")
public class FilterAdmin implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        if (((HttpServletRequest) req).getSession(false) == null) {
                ((HttpServletResponse) resp).sendRedirect("/myFirstWebApp/index.html");
        }
        if (((HttpServletRequest) req).getSession(false) != null) {
           if (!((HttpServletRequest) req).getSession(false).getAttribute("role").equals("admin"))
            ((HttpServletResponse) resp).sendRedirect("/myFirstWebApp/user");
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
