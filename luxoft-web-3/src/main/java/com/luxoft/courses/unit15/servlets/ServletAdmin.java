package com.luxoft.courses.unit15.servlets;

import com.luxoft.courses.unit15.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 30.06.13
 * Time: 10:09
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name = "ServletAdmin")
public class ServletAdmin extends HttpServlet {
    static AtomicLong countGet= new AtomicLong(0);
    static AtomicLong countPost= new AtomicLong(0);
    static AtomicLong countOther= new AtomicLong(0);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        countPost.getAndIncrement();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        countGet.getAndIncrement();
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        writer.println("<TABLE BORDER>");
        writer.println("<TR> <TD>Parameter</TD> <TD>Value</TD>      </TR>");
        writer.println("<TR> <TD>Active Sessions</TD> <TD>"+((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE)).get() +"</TD> </TR>");
        writer.println("<TR> <TD>Active Sessions(ROLE user)</TD> <TD>"+((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE_USER)).get() +"</TD> </TR>");
        writer.println("<TR> <TD>Active Sessions(ROLE admin)</TD> <TD>"+((AtomicLong) getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE_ADMIN)).get() +"</TD> </TR>");
        writer.println("<TR> <TD>Total count of HTTPRequest</TD> <TD>"+(countGet.intValue()+countOther.intValue()+countPost.intValue()) +"</TD>  </TR>");
        writer.println("<TR> <TD>Total count of POST HTTPRequest</TD> <TD>"+ countPost+"</TD>  </TR>");
        writer.println("<TR> <TD>Total count of GET HTTPRequest</TD> <TD>"+ countGet+"</TD> </TR>");
        writer.println("<TR> <TD>Total count of Other HTTPRequest</TD> <TD>"+countOther +"</TD> </TR>");
        writer.println("</TABLE>");
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        countOther.getAndIncrement();
    }
    protected void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        countOther.getAndIncrement();
    }
    protected void doTrace(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        countOther.getAndIncrement();
    }
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        countOther.getAndIncrement();
    }
    protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        countOther.getAndIncrement();
    }







}
