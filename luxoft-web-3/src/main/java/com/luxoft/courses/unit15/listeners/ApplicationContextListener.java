package com.luxoft.courses.unit15.listeners;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.courses.unit15.Constants;
import com.luxoft.courses.unit15.User;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 30.06.13
 * Time: 10:07
 * To change this template use File | Settings | File Templates.
 */
public class ApplicationContextListener implements ServletContextListener {
    ServletContext context;
    Map<String,User> userMap=new HashMap<String, User>();
    @Override
    public void contextInitialized(ServletContextEvent sce) {


        context = sce.getServletContext();
        String jsonUsers =  context.getInitParameter("users");
         loginPassword(jsonUsers);




        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE_ADMIN, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE_USER, new AtomicLong(0));
        sce.getServletContext().setAttribute("userMap",userMap);



    }

    private void loginPassword(String jsonUsers) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<User>>() {
        }.getType();
        List<User> userList = gson.fromJson(jsonUsers, type);

        for (User user : userList) {
            userMap.put(user.getLogin(), user);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE_ADMIN);
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE_USER);

    }
}

