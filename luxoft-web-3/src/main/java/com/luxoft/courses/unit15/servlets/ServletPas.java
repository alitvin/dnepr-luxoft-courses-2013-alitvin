package com.luxoft.courses.unit15.servlets;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.courses.unit15.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 15.06.13
 * Time: 18:42
 * To change this template use File | Settings | File Templates.
 */

@WebServlet(name = "ServletPas")

public class ServletPas extends HttpServlet {




    String role;

    public String getRole() {
        return role;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String paramLogin = request.getParameter("login");
        String paramPassword = request.getParameter("password");

          Map<String,User> userMap = (Map) getServletContext().getAttribute("userMap");

        if (userMap.containsKey(paramLogin)&&userMap.get(paramLogin).getPassword().equals(paramPassword)){
            User user = userMap.get(paramLogin);
                role=user.getRole();
            HttpSession session = request.getSession();
            session.setAttribute("name", paramLogin);
             session.setAttribute("role",role);
             if (role.equals("user"))
            response.sendRedirect("user");
                else
                 response.sendRedirect("admin/sessionData");
        } else {
            response.sendRedirect("index.html?param1=error");
        }
    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
