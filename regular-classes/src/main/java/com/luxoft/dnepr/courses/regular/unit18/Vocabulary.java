package com.luxoft.dnepr.courses.regular.unit18;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class Vocabulary {
    private Set<String> vocabulary = new HashSet<String>();

    public Vocabulary() {
        createVocabulary();
    }

    public Set<String> getVocabulary() {
        return vocabulary;
    }

    private void createVocabulary() {
        String s = "";
        try {
            s = read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        vocabularyAddWords(s);
    }

    public String read() throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader in = new BufferedReader(new FileReader(new File("E:\\Luxoft\\dnepr-luxoft-courses-2013-alitvin\\regular-classes\\src\\main\\java\\com\\luxoft\\dnepr\\courses\\regular\\unit18\\sonnets.txt")));
        String s;
        while ((s = in.readLine()) != null) {
            sb.append(s);
            sb.append("\n");
        }
        in.close();
        return sb.toString();
    }

    public void vocabularyAddWords(String s) {
        StringTokenizer tokenizer = new StringTokenizer(s);
        while (tokenizer.hasMoreTokens()) {
            String string = tokenizer.nextToken();
            if (string.length() > 3)
                vocabulary.add(string.toLowerCase());
        }
    }


    public String random() {
        int i = (int) (Math.random() * vocabulary.size());
        if (vocabulary.size() != 0)
            return (String) vocabulary.toArray()[i];
        else
            return "";

    }
}
