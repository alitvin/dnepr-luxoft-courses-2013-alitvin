package com.luxoft.dnepr.courses.regular.unit6.model;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 01.05.13
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */
public class Employee extends Entity {
    private int salary;

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}

