package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Compiler {
    public static final boolean DEBUG = true;
    private static final String PATTERN = "[+|-|//|*]";

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        System.out.print(byteCode);
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        String a = "";
        String b = "";
        double ad, bd;
        int j;
        char sign = ' ';

        for (int i = 0; i < input.length(); i++)
            if (!Character.isDigit(input.charAt(i)) && (input.charAt(i) != '+' && input.charAt(i) != '-' &&
                    input.charAt(i) != '*' && input.charAt(i) != '/') && input.charAt(i) != ' ' && input.charAt(i) != '.')
                throw new IllegalArgumentException();
        for (int i = 0; i < input.length(); i++)
            if (input.charAt(i) == '+' || input.charAt(i) == '-' ||
                    input.charAt(i) == '*' || input.charAt(i) == '/') {
                sign = input.charAt(i);
                for (j = 0; j < i; j++)
                    a += input.charAt(j);
                for (j = i + 1; j < input.length(); j++)
                    b += input.charAt(j);
            }
        ad = Double.parseDouble(a);
        bd = Double.parseDouble(b);

        addCommand(result, VirtualMachine.PUSH, ad);
        addCommand(result, VirtualMachine.PUSH, bd);
        addCommand(result, VirtualMachine.SWAP);
        if (sign == '+')
            addCommand(result, VirtualMachine.ADD);
        if (sign == '-')
            addCommand(result, VirtualMachine.SUB);
        if (sign == '*')
            addCommand(result, VirtualMachine.MUL);
        if (sign == '/')
            addCommand(result, VirtualMachine.DIV);
        addCommand(result, VirtualMachine.PRINT);
        return result.toByteArray();

    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
       /* while (scanner.hasNext())*/
        {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
