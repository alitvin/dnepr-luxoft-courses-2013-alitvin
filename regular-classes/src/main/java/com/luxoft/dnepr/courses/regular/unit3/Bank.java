package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 23.04.13
 * Time: 11:51
 * To change this template use File | Settings | File Templates.
 */
public class Bank implements BankInterface {
    private Map<Long, UserInterface> users;

    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    public Bank(String expectedJavaVersion) throws IllegalJavaVersionError {
        if (!System.getProperty("java.version").equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(System.getProperty("java.version"), expectedJavaVersion, "");
        }
    }


    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws TransactionException, NoUserFoundException {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.00", otherSymbols);

        users = checkMap(users);
        checkUsers(fromUserId, toUserId);
        String userBlock = users.get(fromUserId).getName();
        if (users.get(fromUserId).getWallet().getStatus().equals(WalletStatus.ACTIVE))
            userBlock = users.get(toUserId).getName();

        moneyTransaction(fromUserId, toUserId, amount, df, userBlock);

    }

    private void moneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount, DecimalFormat df, String userBlock) throws TransactionException {
        try {
            users.get(fromUserId).getWallet().checkWithdrawal(amount);
            users.get(toUserId).getWallet().checkTransfer(amount);
            users.get(fromUserId).getWallet().withdraw(amount);
            users.get(toUserId).getWallet().transfer(amount);
        } catch (WalletIsBlockedException e) {
            throw new TransactionException("User '" + userBlock + "' wallet is blocked");

        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException("User '" + users.get(fromUserId).getName() + "' has insufficient funds (" + df.format(users.get(fromUserId).getWallet().getAmount()) + " < " + df.format(amount) + ")");

        } catch (LimitExceededException e) {
            throw new TransactionException("User '" + users.get(toUserId).getName() + "' wallet limit exceeded (" + df.format(users.get(toUserId).getWallet().getAmount()) + " + " + df.format(amount) + " > " + df.format(users.get(toUserId).getWallet().getMaxAmount()) + ")");

        }
    }

    private void checkUsers(Long fromUserId, Long toUserId) throws NoUserFoundException {
        if (!this.users.containsKey(fromUserId)) {

            throw new NoUserFoundException(fromUserId, "User is not found");
        }
        if (!this.users.containsKey(toUserId)) {
            throw new NoUserFoundException(toUserId, "User is not found");
        }

    }

    private Map<Long, UserInterface> checkMap(Map<Long, UserInterface> users) {
        if (users == null) {
            users = new HashMap<Long, UserInterface>();
        }
        return users;
    }

}
