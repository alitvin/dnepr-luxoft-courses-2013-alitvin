package com.luxoft.dnepr.courses.regular.unit2;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 19.04.13
 * Time: 18:24
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractProduct implements Cloneable {
    private String code;
    private String name;
    private double price;

    protected AbstractProduct(String code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    protected AbstractProduct() {
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String c) {
        this.code = c;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String n) {
        this.name = n;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double p) {
        this.price = p;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractProduct)) return false;

        AbstractProduct that = (AbstractProduct) o;

        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public AbstractProduct clone() throws CloneNotSupportedException {
        AbstractProduct newObject = (AbstractProduct) super.clone();


        newObject.name = new String(this.name);
        newObject.code = new String(this.code);
        return newObject;
    }
}
