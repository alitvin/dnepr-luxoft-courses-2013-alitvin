package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class LimitExceededException extends Exception {

    private Long walletId;
    private BigDecimal amountToTransfer;
    private BigDecimal amountInWallet;

    public LimitExceededException(Long walletId, BigDecimal amountToTransfer, BigDecimal amountInWallet, String message) {
        super(message);
        this.amountInWallet = amountInWallet;
        this.amountToTransfer = amountToTransfer;
        this.walletId = walletId;
    }

    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getAmountToTransfer() {
        return amountToTransfer;
    }

    public BigDecimal getAmountInWallet() {
        return amountInWallet;
    }
}
