package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct implements Product, Cloneable {

    public Beverage(String code, String name, double price, boolean nonAlcoholic) {
        super(code, name, price);
        this.nonAlcoholic = nonAlcoholic;
    }

    public Beverage() {
    }

    private boolean nonAlcoholic;


    public boolean isNonAlcoholic() {
        return this.nonAlcoholic;
    }

    public void setNonAlcoholic(boolean al) {
        this.nonAlcoholic = al;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Beverage)) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        if (nonAlcoholic != beverage.nonAlcoholic) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nonAlcoholic ? 1 : 0);
        return result;
    }


}
