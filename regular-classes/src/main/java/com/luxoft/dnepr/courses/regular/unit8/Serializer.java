package com.luxoft.dnepr.courses.regular.unit8;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 23.05.13
 * Time: 10:07
 * To change this template use File | Settings | File Templates.
 */
public class Serializer {

    public static void serialize(File file, FamilyTree entity) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(entity);
        oos.flush();
        oos.close();

    }

    public static FamilyTree deserialize(File file) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream oin = new ObjectInputStream(fis);
        System.out.println(oin.read());
        FamilyTree familyTree = (FamilyTree) oin.readObject();
        oin.close();
        return familyTree;

    }

}
