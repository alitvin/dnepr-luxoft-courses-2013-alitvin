package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct implements Product, Cloneable {
    private double weight;

    public Bread(String code, String name, double price, double weight) {
        super(code, name, price);
        this.weight = weight;
    }

    public Bread() {
    }

    public void setWeight(double we) {
        this.weight = we;
    }

    public double getWeight() {
        return this.weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bread)) return false;
        if (!super.equals(o)) return false;

        Bread bread = (Bread) o;

        if (Double.compare(bread.weight, weight) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = weight != +0.0d ? Double.doubleToLongBits(weight) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }


}
