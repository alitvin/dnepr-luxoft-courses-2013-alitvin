package com.luxoft.dnepr.courses.regular.unit18;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 26.06.13
 * Time: 13:26
 * To change this template use File | Settings | File Templates.
 */
public class LingusticAnalizator {

    public static void main(String[] args) {
        Vocabulary vocabulary = new Vocabulary();
        UI ui = new UI(vocabulary);
        ui.run();
    }
}
