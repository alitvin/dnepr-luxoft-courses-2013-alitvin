package com.luxoft.dnepr.courses.regular.unit2;


import java.util.Date;


public class Book extends AbstractProduct implements Product, Cloneable {

    private Date date;

    public Book(String code, String name, double price, Date date) {
        super(code, name, price);
        this.date = date;
    }

    public Book() {
    }

    public void setPublicationDate(Date publicationDate) {
        this.date = publicationDate;
    }

    public Date getPublicationDate() {
        return this.date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        if (date != null ? !date.equals(book.date) : book.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public Book clone() throws CloneNotSupportedException {
        Book newObject = (Book) super.clone();
        newObject.date = (Date) this.date.clone();
        return newObject;
    }
}
