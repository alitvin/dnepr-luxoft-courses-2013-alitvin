package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;

import java.util.Map;


/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 07.05.13
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */
public abstract class EntityDaoImpl<E extends Entity> implements IDao<E> {
    Map<Long, Entity> entities = EntityStorage.getEntities();


    @Override
    public E save(E e) throws EntityAlreadyExistException {
        synchronized (entities) {
            if (null == e.getId())
                e.setId(EntityStorage.getNextId());
            if (entities.containsKey(e.getId()))
                throw new EntityAlreadyExistException();
            entities.put(e.getId(), e);
        }

        return e;
    }


    @Override
    public E update(E e) throws EntityNotFoundException {
        synchronized (entities) {
            if (e.getId() == null || !entities.containsKey(e.getId()))
                throw new EntityNotFoundException();
            entities.put(e.getId(), e);
        }
        return e;
    }

    @Override
    public E get(long id) {
        synchronized (entities) {
            if (!entities.containsKey(id))
                return null;
            else
                return (E) entities.get(id);
        }
    }

    @Override
    public boolean delete(long id) {
        synchronized (entities) {
            if (!entities.containsKey(id))
                return false;
            entities.remove(id);
            return true;
        }
    }

}
