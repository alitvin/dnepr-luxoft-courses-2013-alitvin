package com.luxoft.dnepr.courses.regular.unit4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 27.04.13
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
public abstract class EqualSet<E> implements Set<E> {
    private ArrayList<E> collection;

    public EqualSet() {
        this.collection = new ArrayList<E>();
    }

    public EqualSet(Collection<? extends E> collection) {
        if (collection == null)
            this.collection = new ArrayList<E>();
        this.collection.addAll(collection);
    }

    public boolean add(E e) {
        for (E el : collection)
            if (e.equals(el)) {
                return false;
            }
        collection.add(e);
        return true;
    }

    public boolean remove(Object o) {
        for (E el : collection)
            if (el.equals(o)) {
                collection.remove(o);
                return true;
            }
        return false;
    }

    public int size() {
        int count = 0;
        for (E el : collection) {
            count++;
        }
        return count;
    }

    public boolean contains(Object o) {
        for (E el : collection)
            if (el.equals(o)) {

                return true;
            }
        return false;
    }

    public boolean isEmpty() {
        if (this.collection.size() == 0)
            return true;
        return false;
    }

    public Object[] toArray() {
        Object[] objects = new Object[collection.size()];
        int i = 0;
        for (Object o : collection) {
            objects[i++] = o;
        }
        return objects;
    }

    public Iterator<E> iterator() {
        return this.collection.iterator();
    }

    public <T> T[] toArray(T[] a) {
        return collection.toArray(a);
    }

    public boolean containsAll(Collection<?> c) {
        return collection.containsAll(c);
    }

    public boolean addAll(Collection<? extends E> c) {
        boolean added = false;
        if (c != null) {
            return false;
        }
        for (E el : c) {
            if (!collection.contains(el)) {
                collection.add(el);
            }
            added = true;
        }
        return added;
    }

    public boolean retainAll(Collection<?> c) {
        return collection.retainAll(c);
    }

    public boolean removeAll(Collection<?> c) {
        return collection.removeAll(c);
    }

    public void clear() {
        this.collection.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EqualSet)) return false;

        EqualSet equalSet = (EqualSet) o;

        if (collection != null ? !collection.equals(equalSet.collection) : equalSet.collection != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return collection != null ? collection.hashCode() : 0;
    }
}