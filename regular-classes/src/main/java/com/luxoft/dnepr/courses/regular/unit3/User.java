package com.luxoft.dnepr.courses.regular.unit3;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 23.04.13
 * Time: 11:49
 * To change this template use File | Settings | File Templates.
 */
public class User implements UserInterface {

    private Long id;
    private String name;
    private WalletInterface wallet;

    public Long getId() {
        return id;
    }

    public User(Long id, String name, WalletInterface wallet) {
        this.id = id;
        this.name = name;
        this.wallet = wallet;
    }

    public User() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WalletInterface getWallet() {
        return wallet;
    }

    public void setWallet(WalletInterface wallet) {
        this.wallet = wallet;
    }
}
