package com.luxoft.dnepr.courses.regular.unit7_1;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 17.05.13
 * Time: 13:03
 * To change this template use File | Settings | File Templates.
 */
public final class ThreadProducer {

    private ThreadProducer() {
    }

    public static Thread getNewThread() {
        Thread thread = new Thread();
        return thread;
    }

    public static Thread getRunnableThread() {
        Thread thread = new Thread();
        thread.start();
        return thread;
    }


    public static Thread getWaitingThread() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return thread;
    }

    public static Thread getTimedWaitingThread() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    try {
                        wait(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return thread;
    }

    public static Thread getTerminatedThread() {
        Thread thread = new Thread();
        thread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return thread;
    }
}



