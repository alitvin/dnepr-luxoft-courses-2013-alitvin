package com.luxoft.dnepr.courses.regular.unit6.storage;

import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 01.05.13
 * Time: 15:59
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorage {
    private final static Map<Long, Entity> entities = new ConcurrentHashMap<Long, Entity>();

    public EntityStorage() {
    }

    public static Map<Long, Entity> getEntities() {
        return entities;
    }

    public synchronized static Long getNextId() {
        if (entities.isEmpty())
            return Long.valueOf(1);
        return Collections.max(entities.keySet()) + 1;

    }
}
