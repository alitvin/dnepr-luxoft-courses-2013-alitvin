package com.luxoft.dnepr.courses.regular.unit18;

import java.util.Scanner;


public class UI implements Runnable {
    private boolean isPlay = true;
    private Vocabulary vocabulary;

    public UI(Vocabulary vocabulary) {
        this.vocabulary = vocabulary;
    }

    public UI() {
        this.vocabulary = new Vocabulary();
    }

    public void run() {
        while (isPlay) {
            Scanner scanner = new Scanner(System.in);
            String word = vocabulary.random();
            System.out.println(Constants.QUESTION);
            System.out.println(word);
            answer(vocabulary, scanner, word);
        }
    }

    private void answer(Vocabulary vocabulary, Scanner scanner, String word) {
        Boolean flag = false;
        String string;
        string = scanner.next();
        if (validate(string, "exit")) {
            Words.result(vocabulary.getVocabulary().size());
            isPlay = false;
        } else if (validate(string, "help"))
            System.out.println(Constants.HELP);
        else if (isTrueLetter(string))
            flag = string.equalsIgnoreCase("Y");
        else
            System.out.println(Constants.ERROR);
        Words.answer(word, flag);

    }

    private boolean isTrueLetter(String string) {
        if (string != null && (string.equalsIgnoreCase("y") || string.equalsIgnoreCase("n")))
            return true;
        return false;
    }


    private boolean validate(String string, String word) {
        if (string != null && (string.isEmpty() || string.equals(word)))
            return true;
        return false;
    }
}
