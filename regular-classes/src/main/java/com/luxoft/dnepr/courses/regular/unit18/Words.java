package com.luxoft.dnepr.courses.regular.unit18;

import java.util.Vector;

public class Words {
    private static int result = 0;
    private static Vector<String> knownWords = new Vector();
    private static Vector<String> unknownWords = new Vector();

    public static void result(int total) {
        result = total * (knownWords.size() + 1) / (knownWords.size() + unknownWords.size() + 1);
        System.out.println("Your estimated vocabulary is " + result + " words");
    }

    public static int getResult() {
        return result;
    }

    public static Vector<String> getUnknownWords() {
        return unknownWords;
    }

    public static Vector<String> getKnownWords() {
        return knownWords;
    }

    public static void answer(String word, Boolean flag) {
        if (flag)
            knownWords.add(word);
        else
            unknownWords.add(word);
    }

}
