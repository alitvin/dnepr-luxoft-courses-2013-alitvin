package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * <p/>
 * Date: 23.04.13
 * Time: 11:04
 * To change this template use File | Settings | File Templates.
 */
public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        this.id = id;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
    }

    public Wallet() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {

        return amount.setScale(2, RoundingMode.DOWN);
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public WalletStatus getStatus() {
        return status;
    }

    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount.setScale(2, RoundingMode.DOWN);
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        //   - выбрасывает исключение WalletIsBlockedException, если кошелек заблокирован
        //  - выбрасывает исключение InsufficientWalletAmountException, если сумма снятия(amountToWithdraw) больше, чем сумма(amount) в кошельке
        if (status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(id, "User '${user.name}' wallet is blocked");
        }
        if (amountToWithdraw.compareTo(amount) == 1) {
            throw new InsufficientWalletAmountException(id, amountToWithdraw, amount, "User '${user.name}' has insufficient funds (${user.wallet.amount} < ${amountToWithdraw})");
        }

    }

    public void withdraw(BigDecimal amountToWithdraw) {

        this.amount = this.amount.subtract(amountToWithdraw);

    }

    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        //  - выбрасывает исключение WalletIsBlockedException, если кошелек заблокирован
        // - выбрасывает исключение LimitExceededException, если сумма перевода(amountToTransfer) + сумма(amount) в кошельке превышают лимит кошелька (maxAmount)
        if (status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(id, "User '${user.name}' wallet is blocked");
        }
        BigDecimal maxAmountToTransfer = amount;
        maxAmountToTransfer = maxAmountToTransfer.add(amountToTransfer);
        if (maxAmountToTransfer.compareTo(maxAmount) == 1) {
            throw new LimitExceededException(id, amountToTransfer, amount, "User '${user.name}' wallet limit exceeded (${user.wallet.amount} + ${amountToWithdraw} > ${user.wallet.maxAmount})");
        }

    }

    public void transfer(BigDecimal amountToTransfer) {

        this.amount = this.amount.add(amountToTransfer);

    }

}
