package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    List<CompositeProduct> compositeProductList = new ArrayList<CompositeProduct>();

    class SortedByPrice implements Comparator<Product> {

        public int compare(Product obj1, Product obj2) {

            double price1 = ((Product) obj1).getPrice();
            double price2 = ((Product) obj2).getPrice();

            if (price1 > price2)
                return -1;
            else if (price1 < price2)
                return 1;
            else
                return 0;
        }
    }

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        int count = 0;
        for (CompositeProduct compositeProduct : compositeProductList) {
            if (compositeProduct.getProduct().equals(product)) {
                compositeProduct.add(product);
                count++;
            }
        }
        if (count == 0) {
            CompositeProduct compositeProduct = new CompositeProduct();
            compositeProduct.add(product);
            compositeProductList.add(compositeProduct);
        }
    }


    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double totalCost = 0;
        for (int i = 0; i < compositeProductList.size(); i++) {
            totalCost += this.compositeProductList.get(i).getPrice();
        }
        //System.out.print(compositeProductList.get(i).getName()+"  "+compositeProductList.get(i).getPrice()); }
        return totalCost;
    }


    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        List<Product> productSortedList = new ArrayList<Product>();
        for (int i = 0; i < compositeProductList.size(); i++)
            productSortedList.add(compositeProductList.get(i));
        Collections.sort(productSortedList, new SortedByPrice());
        return productSortedList;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

}
