package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<Product>();


    public CompositeProduct() {
    }

    public Product getProduct() {
        if (childProducts.isEmpty()) return null;
        return childProducts.get(0);
    }


    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        if (this.childProducts.isEmpty())
            return null;
        else
            return this.childProducts.get(0).getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        if (this.childProducts.isEmpty())
            return null;
        else
            return this.childProducts.get(0).getName();
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        int i;
        double price = 0;
        if (this.getAmount() == 1)
            price += childProducts.get(0).getPrice();
        else if (this.getAmount() == 2) {
            price += 0.95 * (childProducts.get(0).getPrice() + childProducts.get(1).getPrice());
        } else {
            for (i = 0; i < this.getAmount(); i++)
                price += childProducts.get(i).getPrice();
            price *= 0.9;
        }
        return price;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
