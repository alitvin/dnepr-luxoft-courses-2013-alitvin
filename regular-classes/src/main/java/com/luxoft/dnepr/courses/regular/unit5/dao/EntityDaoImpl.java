package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exceptions.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exceptions.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Collections;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 07.05.13
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */
public abstract class EntityDaoImpl<E extends Entity> implements IDao<E> {
    Map<Long, Entity> entities = EntityStorage.getEntities();

    @Override
    public E save(E e) throws UserAlreadyExist {

        if (null == e.getId())
            e.setId(newId(entities));
        if (entities.containsKey(e.getId()))
            throw new UserAlreadyExist();
        entities.put(e.getId(), e);
        return e;
    }

    private Long newId(Map<Long, Entity> entities) {
        if (entities.isEmpty())
            return Long.valueOf(1);
        return Collections.max(entities.keySet()) + 1;

    }

    @Override
    public E update(E e) throws UserNotFound {
        if (e.getId() == null || !entities.containsKey(e.getId()))
            throw new UserNotFound();
        entities.put(e.getId(), e);
        return e;
    }

    @Override
    public E get(long id) {
        if (!entities.containsKey(id))
            return null;
        else
            return (E) entities.get(id);
    }

    @Override
    public boolean delete(long id) {
        if (!entities.containsKey(id))
            return false;
        entities.remove(id);
        return true;
    }

}
