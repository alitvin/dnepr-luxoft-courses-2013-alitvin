package com.luxoft.dnepr.courses.regular.unit8;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Date;

public class Person implements Externalizable {

    private String name;
    private Gender gender;
    private String ethnicity;
    private Date birthDate;
    private Person father;
    private Person mother;

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(writeText(out, this));
    }

    public String writeText(ObjectOutput out, Person person) throws IOException {
        StringBuilder sb = new StringBuilder();

        sb.append("{\"name\":\"");
        sb.append(person.getName());
        sb.append("\",\"gender\":\"");
        sb.append(person.getGender().toString());
        sb.append("\",\"ethnicity\":\"");
        sb.append(person.getEthnicity());
        sb.append("\",\"birthDate\":\"");
        sb.append(person.getBirthDate().toString());
        sb.append("\"");

        if (person.getFather() != null) {
            sb.append(",\"father\":");
            sb.append(writeText(out, person.getFather()));
        }

        if (person.getMother() != null) {
            sb.append(",\"mother\":");
            sb.append(writeText(out, person.getMother()));
        }
        sb.append("}");

        return sb.toString();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }
}
