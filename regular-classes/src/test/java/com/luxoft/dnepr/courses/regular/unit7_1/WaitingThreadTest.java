package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 18.05.13
 * Time: 8:55
 * To change this template use File | Settings | File Templates.
 */
public class WaitingThreadTest {
    @Test
    public void getWaitingThreadTest() {
        assertEquals(Thread.State.WAITING, ThreadProducer.getWaitingThread().getState());
    }
}
