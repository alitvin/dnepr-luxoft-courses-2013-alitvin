package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 24.04.13
 * Time: 14:30
 * To change this template use File | Settings | File Templates.
 */
public class BankTest {

    User u1 = new User();
    User u2 = new User();
    User u3 = new User();
    Bank b = new Bank(System.getProperty("java.version"));
    Map<Long, UserInterface> users = new HashMap<Long, UserInterface>();
    Wallet w1 = new Wallet();
    Wallet w2 = new Wallet();
    Wallet w3 = new Wallet();


    @Test
    public void makeMoneyTransaction() throws NoUserFoundException, TransactionException {

        w1.setAmount(BigDecimal.valueOf(1500.0001));
        w1.setId(Long.valueOf(1));
        w1.setMaxAmount(BigDecimal.valueOf(4100));
        w1.setStatus(WalletStatus.ACTIVE);
        u1.setId(Long.valueOf(1));
        u1.setName("Vasia");
        u1.setWallet(w1);

        w2.setAmount(BigDecimal.valueOf(1000));
        w2.setId(Long.valueOf(2));
        w2.setMaxAmount(BigDecimal.valueOf(2000));
        w2.setStatus(WalletStatus.ACTIVE);
        u2.setId(Long.valueOf(2));
        u2.setName("Petia");
        u2.setWallet(w2);

        w3.setAmount(BigDecimal.valueOf(100));
        w3.setId(Long.valueOf(3));
        w3.setMaxAmount(BigDecimal.valueOf(2000));
        w3.setStatus(WalletStatus.BLOCKED);
        u3.setId(Long.valueOf(3));
        u3.setName("Masha");
        u3.setWallet(w3);

        users.put(u1.getId(), u1);
        users.put(u2.getId(), u2);
        users.put(u3.getId(), u3);
        b.setUsers(users);


        try {
            b.makeMoneyTransaction(Long.valueOf(7), u2.getId(), BigDecimal.valueOf(1500));
        } catch (NoUserFoundException e) {
            Assert.assertEquals("User is not found", e.getMessage());
        }


        try {
            b.makeMoneyTransaction(u1.getId(), u2.getId(), BigDecimal.valueOf(1500));

        } catch (TransactionException e) {
            Assert.assertEquals("User 'Petia' wallet limit exceeded (1000.00 + 1500.00 > 2000.00)", e.getMessage());
        }


        try {
            b.makeMoneyTransaction(u2.getId(), u1.getId(), BigDecimal.valueOf(2000));

        } catch (TransactionException e) {
            Assert.assertEquals("User 'Petia' has insufficient funds (1000.00 < 2000.00)", e.getMessage());
        }

        try {
            b.makeMoneyTransaction(u3.getId(), u1.getId(), BigDecimal.valueOf(2000));

        } catch (TransactionException e) {
            Assert.assertEquals("User 'Masha' wallet is blocked", e.getMessage());
        }

        try {
            b.makeMoneyTransaction(u1.getId(), u3.getId(), BigDecimal.valueOf(1000));

        } catch (TransactionException e) {
            Assert.assertEquals("User 'Masha' wallet is blocked", e.getMessage());
        }

        b.makeMoneyTransaction(u1.getId(), u2.getId(), BigDecimal.valueOf(100.00));
        assertEquals("1400.00", users.get(u1.getId()).getWallet().getAmount().toString());
        assertEquals(BigDecimal.valueOf(1100).setScale(2, RoundingMode.DOWN), users.get(u2.getId()).getWallet().getAmount());
        b.makeMoneyTransaction(u2.getId(), u1.getId(), BigDecimal.valueOf(1100));
        Assert.assertEquals(BigDecimal.valueOf(2500.00).setScale(2, RoundingMode.DOWN), users.get(u1.getId()).getWallet().getAmount());
        Assert.assertEquals(BigDecimal.valueOf(0.00).setScale(2, RoundingMode.DOWN), users.get(u2.getId()).getWallet().getAmount());


    }
}
