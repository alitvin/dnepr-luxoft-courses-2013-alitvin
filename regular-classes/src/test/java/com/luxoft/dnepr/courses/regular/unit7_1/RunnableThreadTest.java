package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 17.05.13
 * Time: 14:32
 * To change this template use File | Settings | File Templates.
 */
public class RunnableThreadTest {
    @Test
    public void getRunnableThreadTest() {
        assertEquals(Thread.State.RUNNABLE, ThreadProducer.getRunnableThread().getState());
    }
}
