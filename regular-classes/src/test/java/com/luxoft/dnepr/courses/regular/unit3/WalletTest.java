package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 24.04.13
 * Time: 18:37
 * To change this template use File | Settings | File Templates.
 */
public class WalletTest {
    Wallet w1 = new Wallet();
    Wallet w2 = new Wallet();
    Wallet w3 = new Wallet();

    @Test
    public void withdraw() {
        w1.setAmount(BigDecimal.valueOf(1500));
        w2.setAmount(BigDecimal.valueOf(1000));
        w3.setAmount(BigDecimal.valueOf(2000));
        w1.withdraw(BigDecimal.valueOf(1000));
        w2.withdraw(BigDecimal.valueOf(1000));
        w3.withdraw(BigDecimal.valueOf(1000));
        assertEquals(BigDecimal.valueOf(500.00).setScale(2, RoundingMode.DOWN), w1.getAmount());
        assertEquals(BigDecimal.valueOf(0.00).setScale(2, RoundingMode.DOWN), w2.getAmount());
        assertEquals(BigDecimal.valueOf(1000.00).setScale(2, RoundingMode.DOWN), w3.getAmount());
    }

    @Test
    public void transfer() {
        w1.setAmount(BigDecimal.valueOf(1500));
        w2.setAmount(BigDecimal.valueOf(1000));
        w3.setAmount(BigDecimal.valueOf(2000));
        w1.transfer(BigDecimal.valueOf(1000));
        w2.transfer(BigDecimal.valueOf(1000));
        w3.transfer(BigDecimal.valueOf(1000));
        assertEquals(BigDecimal.valueOf(2500).setScale(2, RoundingMode.DOWN), w1.getAmount());
        assertEquals(BigDecimal.valueOf(2000).setScale(2, RoundingMode.DOWN), w2.getAmount());
        assertEquals(BigDecimal.valueOf(3000).setScale(2, RoundingMode.DOWN), w3.getAmount());
    }

    @Test
    public void checkWithdrawal() {
        w1.setAmount(BigDecimal.valueOf(1500));
        w1.setId(Long.valueOf(1));
        w1.setMaxAmount(BigDecimal.valueOf(2100));
        w1.setStatus(WalletStatus.ACTIVE);

        try {
            w1.checkWithdrawal(BigDecimal.valueOf(5000));

        } catch (WalletIsBlockedException e) {
            Assert.assertEquals("User '${user.name}' wallet is blocked", e.getMessage());
        } catch (InsufficientWalletAmountException e) {
            Assert.assertEquals("User '${user.name}' has insufficient funds (${user.wallet.amount} < ${amountToWithdraw})", e.getMessage());  //To change body of catch statement use File | Settings | File Templates.
        }

        w2.setAmount(BigDecimal.valueOf(1500));
        w2.setId(Long.valueOf(1));
        w2.setMaxAmount(BigDecimal.valueOf(2100));
        w2.setStatus(WalletStatus.BLOCKED);

        try {
            w2.checkWithdrawal(BigDecimal.valueOf(5000));

        } catch (WalletIsBlockedException e) {
            Assert.assertEquals("User '${user.name}' wallet is blocked", e.getMessage());
        } catch (InsufficientWalletAmountException e) {
            Assert.assertEquals("User '${user.name}' has insufficient funds (${user.wallet.amount} < ${amountToWithdraw})", e.getMessage());  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void checkTransfer() {
        w1.setAmount(BigDecimal.valueOf(1500));
        w1.setId(Long.valueOf(1));
        w1.setMaxAmount(BigDecimal.valueOf(2100));
        w1.setStatus(WalletStatus.ACTIVE);


        try {
            w1.checkTransfer(BigDecimal.valueOf(5000));
        } catch (WalletIsBlockedException e) {
            Assert.assertEquals("User '${user.name}' wallet is blocked", e.getMessage());
        } catch (LimitExceededException e) {
            Assert.assertEquals("User '${user.name}' wallet limit exceeded (${user.wallet.amount} + ${amountToWithdraw} > ${user.wallet.maxAmount})", e.getMessage());
        }


        w2.setAmount(BigDecimal.valueOf(1500));
        w2.setId(Long.valueOf(1));
        w2.setMaxAmount(BigDecimal.valueOf(2100));
        w2.setStatus(WalletStatus.BLOCKED);


        try {
            w2.checkTransfer(BigDecimal.valueOf(5000));
        } catch (WalletIsBlockedException e) {
            Assert.assertEquals("User '${user.name}' wallet is blocked", e.getMessage());
        } catch (LimitExceededException e) {
            Assert.assertEquals("User '${user.name}' wallet limit exceeded (${user.wallet.amount} + ${amountToWithdraw} > ${user.wallet.maxAmount})", e.getMessage());
        }

    }

}
