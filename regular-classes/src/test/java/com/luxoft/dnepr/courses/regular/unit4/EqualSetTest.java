package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 27.04.13
 * Time: 23:49
 * To change this template use File | Settings | File Templates.
 */
public class EqualSetTest {
    EqualSet<String> set = new EqualSet<String>() {

    };
    String s1 = "dsfg";
    String s2 = "jkm";
    String s3 = "ijuhnmk";
    String s4 = "ijuhnmk";


    @Test
    public void add() {
        set.add(s1);
        set.add(s2);
        set.add(s3);
        set.add(s4);

        assertEquals("dsfg", set.toArray()[0]);
        assertEquals("jkm", set.toArray()[1]);
        assertEquals("ijuhnmk", set.toArray()[2]);
        assertEquals(3, set.size());


    }

    @Test
    public void size() {
        set.add(s1);
        assertEquals(1, set.size());
        set.add(s2);
        assertEquals(2, set.size());
        set.add(s3);
        assertEquals(3, set.size());
        set.add(s4);
        assertEquals(3, set.size());
    }

    @Test
    public void remove() {
        set.add(s1);
        set.add(s2);
        set.add(s3);
        set.add(s4);
        set.remove(s1);
        assertEquals("jkm", set.toArray()[0]);
        assertEquals("ijuhnmk", set.toArray()[1]);
        assertEquals(2, set.size());
        set.remove(s4);
        assertEquals("jkm", set.toArray()[0]);
        assertEquals(1, set.size());
    }

    @Test
    public void isEmpty() {
        assertTrue(set.isEmpty());
        set.add(s1);
        assertFalse(set.isEmpty());
        set.remove(s1);
        assertTrue(set.isEmpty());
        assertEquals(0, set.size());
    }

    @Test
    public void contains() {
        set.add(s1);
        set.add(s2);
        set.add(s3);
        set.add(s4);
        assertTrue(set.contains(s1));
        assertTrue(set.contains(s2));
        assertTrue(set.contains(s3));
        assertTrue(set.contains(s4));
        assertTrue(set.contains("ijuhnmk"));

        set.remove(s1);
        assertFalse(set.contains(s1));
    }

    @Test
    public void clear() {
        assertTrue(set.isEmpty());
        set.add(s1);
        set.add(s2);
        set.add(s3);
        assertFalse(set.isEmpty());
        set.clear();
        assertTrue(set.isEmpty());
        assertEquals(0, set.size());
    }

    @Test
    public void iterator() {
        set.add(s1);
        set.add(s2);
        set.add(s3);
        Iterator iter = set.iterator();
        while (iter.hasNext()) {
            assertTrue(set.contains(iter.next()));
        }
    }

}