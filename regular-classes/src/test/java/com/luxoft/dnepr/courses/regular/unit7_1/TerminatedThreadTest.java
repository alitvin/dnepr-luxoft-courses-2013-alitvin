package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 17.05.13
 * Time: 23:43
 * To change this template use File | Settings | File Templates.
 */
public class TerminatedThreadTest {
    @Test
    public void TerminatedThreadTest() {
        assertEquals(Thread.State.TERMINATED, ThreadProducer.getTerminatedThread().getState());
    }
}
