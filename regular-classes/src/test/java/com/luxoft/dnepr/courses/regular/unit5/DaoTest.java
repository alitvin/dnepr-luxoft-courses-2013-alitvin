package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exceptions.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exceptions.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 07.05.13
 * Time: 18:17
 * To change this template use File | Settings | File Templates.
 */
public class DaoTest {
    IDao<Redis> dao = new RedisDaoImpl();
    EntityStorage e = new EntityStorage();

    @Before
    public void begin() {
        Employee e1 = new Employee();
        Redis r1 = new Redis();
        e1.setSalary(100);
        e1.setId(Long.valueOf(1));
        r1.setWeight(10);
        r1.setId(Long.valueOf(2));
        e.getEntities().clear();
        e.getEntities().put(Long.valueOf(1), e1);
        e.getEntities().put(Long.valueOf(2), r1);
    }

    @Test
    public void save() throws UserAlreadyExist {
        Redis r2 = new Redis();
        r2.setWeight(300);
        r2.setId(Long.valueOf(3));
        assertEquals(2, e.getEntities().size());
        dao.save(r2);
        assertEquals(3, e.getEntities().size());
        Redis r3 = new Redis();
        r3.setWeight(3000);
        dao.save(r3);
        assertEquals(4, e.getEntities().size());
        assertTrue(e.getEntities().containsKey(Long.valueOf(4)));
    }

    @Test
    public void update() throws UserNotFound {
        Redis r1 = new Redis();
        r1.setWeight(300);
        r1.setId(Long.valueOf(1));
        dao.update(r1);
        Assert.assertEquals(Long.valueOf(1), r1.getId());
        Assert.assertEquals(300, r1.getWeight());
    }

    @Test
    public void get() {
        Redis r1 = dao.get(2);
        Assert.assertEquals(Long.valueOf(2), r1.getId());
        Assert.assertEquals(10, r1.getWeight());
    }

    @Test
    public void deleteTest() {
        assertEquals(2, e.getEntities().size());
        assertTrue(dao.delete(Long.valueOf(2)));
        assertEquals(1, e.getEntities().size());
        assertTrue(dao.delete(Long.valueOf(1)));
        assertTrue(e.getEntities().isEmpty());
        assertFalse(dao.delete(Long.valueOf(2)));

    }


}
