package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 17.05.13
 * Time: 14:14
 * To change this template use File | Settings | File Templates.
 */
public class NewThreadTest {


    @Test
    public void getNewThreadTest() {
        assertEquals(Thread.State.NEW, ThreadProducer.getNewThread().getState());
    }


}
