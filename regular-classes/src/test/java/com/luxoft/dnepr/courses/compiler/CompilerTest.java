package com.luxoft.dnepr.courses.compiler;

import org.junit.Test;

public class CompilerTest extends AbstractCompilerTest {

	@Test
	public void testSimple() {
		assertCompiled(4, "2+2");
		assertCompiled(5, " 2 + 3 ");
		assertCompiled(1, "2 - 1 ");
		assertCompiled(6, " 2 * 3 ");
		assertCompiled(4.5, "  9 /2 ");
        assertCompiled(1,"1+0");
        assertCompiled(9,"3*3");
        assertCompiled(10,"      2   *   5   ");
        assertCompiled(0.1,"1/10");
        assertCompiled(36,"6*6");
        assertCompiled(-5,"15-20");
        assertCompiled(0,"0*0");
        assertCompiled(0.5,"0.2+0.3");
	}
	
	@Test
	public void testComplex() { /*
		assertCompiled(12, "  (2 + 2 ) * 3 ");
		assertCompiled(8.5, "  2.5 + 2 * 3 ");
		assertCompiled(8.5, "  2 *3 + 2.5"); */
	}
}
