package com.luxoft.dnepr.courses.regular.unit8;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 23.05.13
 * Time: 13:14
 * To change this template use File | Settings | File Templates.
 */
public class SerializerTest {
    @Test
    public void q() {
        Person person = new Person();
        person.setBirthDate(new Date());
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");
        Person father = new Person();
        father.setBirthDate(new Date());
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        person.setFather(father);
        FamilyTree obj = new FamilyTree(person);
        File f = new File("E:/q.JSON");
        try {
            Serializer.serialize(f, obj);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            Serializer.deserialize(f);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

        }
    }
}
