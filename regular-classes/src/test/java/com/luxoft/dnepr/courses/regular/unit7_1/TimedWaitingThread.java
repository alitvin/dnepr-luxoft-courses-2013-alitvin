package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 18.05.13
 * Time: 10:02
 * To change this template use File | Settings | File Templates.
 */
public class TimedWaitingThread {
    @Test
    public void getTimedWaitingThreadTest() {
        assertEquals(Thread.State.TIMED_WAITING, ThreadProducer.getTimedWaitingThread().getState());
    }
}
