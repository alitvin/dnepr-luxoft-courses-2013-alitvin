package com.luxoft.dnepr.courses.regular.unit18;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 27.06.13
 * Time: 9:59
 * To change this template use File | Settings | File Templates.
 */
public class VocabularyTest {
    @Test
    public void vocabularyAddWords(){
        Vocabulary vocabulary= new Vocabulary();
        vocabulary.getVocabulary().clear();
        assertEquals(0, vocabulary.getVocabulary().size());
        vocabulary.vocabularyAddWords("qwert qwerty sdfg sdfgh e dd fg");
        assertEquals(4,vocabulary.getVocabulary().size());
    }

    public void random(){
        Vocabulary vocabulary= new Vocabulary();
        String word1=vocabulary.random();
        String word2=vocabulary.random();
        String word3=vocabulary.random();
        assertTrue(vocabulary.getVocabulary().contains(word1));
        assertTrue(vocabulary.getVocabulary().contains(word2));
        assertTrue(vocabulary.getVocabulary().contains(word3));
    }
}
