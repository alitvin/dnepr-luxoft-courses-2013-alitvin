package com.luxoft.dnepr.courses.regular.unit6;


import com.luxoft.dnepr.courses.regular.unit6.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.model.Employee;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 18.05.13
 * Time: 11:15
 * To change this template use File | Settings | File Templates.
 */
public class DaoSaveTest {
    Map<Long, Entity> entities = EntityStorage.getEntities();
    IDao<Employee> employeeDao = new EmployeeDaoImpl();

    @Test
    public void testSave() throws EntityAlreadyExistException {
        ThreadGroup threadGroup = new ThreadGroup("");
        for (int i = 0; i < 100; i++) {
            new Thread(threadGroup, new ThreadTest()).start();
        }
        while (threadGroup.activeCount() != 0) {
        }
        Assert.assertEquals(500, entities.size());
    }

    class ThreadTest implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                employeeDao.save(new Employee());
                //System.out.print(entities.size());
            }
        }
    }

}
