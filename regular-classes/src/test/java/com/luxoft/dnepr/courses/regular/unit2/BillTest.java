package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BillTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testAppend() throws Exception {
        Bill bill = new Bill();
        //append a piece of bread
        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        assertEquals(1, bill.getProducts().size());

        //append the same piece of bread
        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        assertEquals(1, bill.getProducts().size());

        //append a beverage
        bill.append(factory.createBeverage("bev", "Cola", 10, true));
        assertEquals(2, bill.getProducts().size());

        //append another beverage
        bill.append(factory.createBeverage("bev2", "Martini", 100, false));
        assertEquals(3, bill.getProducts().size());
    }

    @Test
    public void testSummarize() throws Exception {
        Bill bill = new Bill();
        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        bill.append(factory.createBread("br", "Bread", 10, 1.0));

        Beverage cola = factory.createBeverage("bev", "Cola", 10, true);
        bill.append(cola);
        bill.append((Beverage)cola.clone());
        Beverage colaExpensive = factory.createBeverage("bev", "Cola", 50, true);
        bill.append(colaExpensive);

        bill.append(factory.createBook("book", "Java", 100, new Date()));
        assertEquals( (10+10)*0.95 + (10 + 10 + 50) * 0.9 + 100, bill.summarize(), 0 );


        Bill bill1 = new Bill();
        bill1.append(factory.createBread("br", "White bread", 4, 0.6));
        bill1.append(factory.createBread("br", "White bread", 4, 0.6));
        bill1.append(factory.createBread("br", "Black bread",5 , 0.7));
        bill1.append(factory.createBread("br", "Black bread",3 , 0.4));


        bill1.append(factory.createBeverage("bev", "Juice", 20, false));
        bill1.append(factory.createBeverage("bev", "Water", 6, false));
        bill1.append(factory.createBeverage("bev", "Beer", 15, true));
        bill1.append(factory.createBeverage("bev", "Beer", 15, true));

        bill1.append(factory.createBook("book", "The fools is die", 70, new Date()));
        assertEquals(70+(15+15)*0.95+20+6+(4+4)*0.95+5+3 , bill1.summarize(), 0 );
    }

    @Test
    public void testGetProducts() throws Exception {
        Bill bill = new Bill();

        assertEquals(0, bill.getProducts().size());

        bill.append(factory.createBread("br", "Bread", 10, 1.0));
        bill.append(factory.createBread("br", "Bread", 10, 1.0));

        bill.append(factory.createBeverage("bev", "Cola", 12, true));

        bill.append(factory.createBook("book", "Java", 100, new Date()));

        List<Product> groupedAndSortedProducts = bill.getProducts();
        assertEquals(3, groupedAndSortedProducts.size());

        //test sorting - by grouped price, descending
        assertTrue(
                groupedAndSortedProducts.get(0).getPrice() >= groupedAndSortedProducts.get(1).getPrice() &&
                        groupedAndSortedProducts.get(1).getPrice() >= groupedAndSortedProducts.get(2).getPrice()
        );

        assertEquals("Java", groupedAndSortedProducts.get(0).getName());
        assertEquals("Bread", groupedAndSortedProducts.get(1).getName());
        assertEquals("Cola", groupedAndSortedProducts.get(2).getName());

        Bill bill1 = new Bill();
        bill1.append(factory.createBread("br", "White bread", 4, 0.6));
        bill1.append(factory.createBread("br", "White bread", 4, 0.6));
        bill1.append(factory.createBread("br", "Black bread",5 , 0.7));
        bill1.append(factory.createBread("br", "Black bread",3 , 0.4));


        bill1.append(factory.createBeverage("bev", "Juice", 20, false));
        bill1.append(factory.createBeverage("bev", "Water", 6, false));
        bill1.append(factory.createBeverage("bev", "Beer", 15, true));
        bill1.append(factory.createBeverage("bev", "Beer", 15, true));

        bill1.append(factory.createBook("book", "The fools is die", 70, new Date()));

        List<Product> groupedAndSortedProducts1 = bill1.getProducts();
        assertEquals(7, groupedAndSortedProducts1.size());

        assertTrue(
            groupedAndSortedProducts1.get(0).getPrice() >= groupedAndSortedProducts1.get(1).getPrice() &&
                    groupedAndSortedProducts1.get(1).getPrice() >= groupedAndSortedProducts1.get(2).getPrice()  &&
                groupedAndSortedProducts1.get(2).getPrice() >= groupedAndSortedProducts1.get(3).getPrice() &&
                        groupedAndSortedProducts1.get(3).getPrice() >= groupedAndSortedProducts1.get(4).getPrice() &&
                groupedAndSortedProducts1.get(4).getPrice() >= groupedAndSortedProducts1.get(5).getPrice() &&
                        groupedAndSortedProducts1.get(5).getPrice() >= groupedAndSortedProducts1.get(6).getPrice()


        );
    }
}
