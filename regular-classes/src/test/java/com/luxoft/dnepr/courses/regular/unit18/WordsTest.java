package com.luxoft.dnepr.courses.regular.unit18;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Антон
 * Date: 26.06.13
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
public class WordsTest {

    @Test
    public void answer(){
        Words.answer("sonnet1",true);
        Words.answer("sonnet2",false);
        Words.answer("sonnet3",true);
        assertEquals(2,Words.getKnownWords().size());
        assertEquals(1,Words.getUnknownWords().size());
    }

    @Test
    public  void result(){
        Words.answer("sonnet1",true);
        Words.answer("sonnet2",false);
        Words.answer("sonnet3",true);
        Words.answer("sonnet3",true);
        Words.answer("sonnet3",true);
        Words.answer("sonnet3",false);
        Words.result(10);
        assertEquals(10*5/7,Words.getResult());
    }
}
