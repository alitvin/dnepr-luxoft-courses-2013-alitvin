SELECT DISTINCT  makers.maker_name FROM pc
JOIN product ON pc.model=product.model
JOIN makers ON product.maker_id=makers.maker_id
WHERE makers.maker_name NOT IN (
SELECT DISTINCT  makers.maker_name FROM laptop
JOIN product ON laptop.model=product.model
JOIN makers ON product.maker_id=makers.maker_id)