SELECT AVG(pc.hd) AS avg_hd FROM pc
JOIN product ON pc.model=product.model
JOIN makers ON product.maker_id=makers.maker_id
WHERE makers.maker_id IN (
SELECT maker_id FROM product
WHERE product.type='printer')