SELECT DISTINCT   maker_id, AVG(screen) as avg_size FROM laptop
JOIN product ON laptop.model = product.model
WHERE  maker_id IN (SELECT maker_id FROM product WHERE type = 'Laptop')
GROUP BY maker_id
ORDER BY avg_size;