SELECT DISTINCT maker_id FROM product
WHERE product.type='printer'
AND maker_id IN (
SELECT  maker_id FROM product
JOIN pc ON product.model=pc.model
WHERE pc.hd=(SELECT MAX(pc.hd) FROM pc
WHERE model IN
(SELECT model FROM pc
WHERE model IN (
SELECT model FROM pc
WHERE pc.ram=(SELECT MIN(pc.ram) FROM pc))
)))
