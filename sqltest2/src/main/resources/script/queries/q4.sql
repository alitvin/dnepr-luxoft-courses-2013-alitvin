 select Distinct makers.maker_name from product
 join makers on product.maker_id = makers.maker_id
 join printer on product.model = printer.model
ORDER BY makers.maker_name DESC
