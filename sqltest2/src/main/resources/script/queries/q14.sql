SELECT makers.maker_name, printer.price from testdb.printer
JOIN testdb.product ON printer.model=product.model
JOIN testdb.makers ON product.maker_id=makers.maker_id
WHERE (printer.color='y')
HAVING MIN(printer.price)
