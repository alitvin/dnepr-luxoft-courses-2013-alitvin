DROP INDEX ind_pc_price ON pc;
DROP INDEX ind_laptop_price ON laptop;
DROP INDEX ind_printer_price ON printer;

DROP TABLE IF EXISTS pc;
DROP TABLE IF EXISTS laptop;
DROP TABLE IF EXISTS printer;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS makers;

