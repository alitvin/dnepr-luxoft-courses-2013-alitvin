CREATE TABLE IF NOT EXISTS makers (
  maker_id     INT,
  maker_name   VARCHAR(50) NOT NULL,
  maker_adress VARCHAR(200),
  CONSTRAINT pc_makers PRIMARY KEY (maker_id)
);

INSERT INTO makers (maker_id, maker_name, maker_adress) VALUES (1, 'A', 'AdressA');
INSERT INTO makers (maker_id, maker_name, maker_adress) VALUES (2, 'B', 'AdressB');
INSERT INTO makers (maker_id, maker_name, maker_adress) VALUES (3, 'C', 'AdressC');
INSERT INTO makers (maker_id, maker_name, maker_adress) VALUES (4, 'D', 'AdressD');
INSERT INTO makers (maker_id, maker_name, maker_adress) VALUES (5, 'E', 'AdressE');

ALTER TABLE product DROP maker;
ALTER TABLE product ADD maker_id INT NOT NULL;
ALTER TABLE product ADD CONSTRAINT fk_product_makers FOREIGN KEY (maker_id) REFERENCES makers (maker_id);

CREATE TABLE IF NOT EXISTS printer_type (
  type_id   INT,
  type_name VARCHAR(50) NOT NULL,
  CONSTRAINT pk_printer_type PRIMARY KEY (type_id)
);

INSERT INTO printer_type (type_id, type_name) VALUES (1, 'Matrix');
INSERT INTO printer_type (type_id, type_name) VALUES (2, 'Laser');
INSERT INTO printer_type (type_id, type_name) VALUES (3, 'Jet');

ALTER TABLE printer DROP type;
ALTER TABLE printer ADD type_id INT NOT NULL;
ALTER TABLE printer ADD CONSTRAINT fk_printer_type FOREIGN KEY (type_id) REFERENCES printer_type (type_id);
ALTER TABLE printer ALTER COLUMN color SET DEFAULT 'y' AND NOT NULL;

CREATE INDEX ind_pc_price ON pc (price);
CREATE INDEX ind_laptop_price ON laptop (price);
CREATE INDEX ind_printer_price ON printer (price);